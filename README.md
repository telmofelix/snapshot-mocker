# What is snapshot mocker
It is a set of simple scripts to generate a sample snapshot entry that can be used for example for Optima

# How to use

To generate a sample snapshot:

* edit the `transform.sh` in the snapshot-sync directory and set up the correct env variables

```
# environment variables to change
export S3_PATH="snapshots-s3/2019/10/24/v2"
export CLIENT_KEY="pPBFRdxAR61DXgGaYvvIPWQ7pAaq8QQJ
export MAX_FILES="10"
```

* run `transform.sh` in the right directory, e.g.

```
cd snapshots-sync
bash transform.sh
```

* you should have now a directory called `snapshots-s3` that contains the generated sample files, we need to sync that with the target bucket, for example

```
cd snapshots-s3/2019/10/24
aws s3 sync . s3://boxever-labs-paddypower-snapshot-subset/snapshots/2019/10/24/
```
