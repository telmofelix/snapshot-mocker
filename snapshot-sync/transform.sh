#!/bin/bash

# environment variables to change
export S3_PATH="snapshots-s3/2019/10/24/v2"
export CLIENT_KEY="pPBFRdxAR61DXgGaYvvIPWQ7pAaq8QQJ"
export MAX_FILES="10"

mkdir -p ${S3_PATH}/{events,guests,sessions}/${CLIENT_KEY}

export EVENTS_LIST=$(find snapshots-paddypower -type f -name "part*" | grep events -m ${MAX_FILES})
export GUESTS_LIST=$(find snapshots-paddypower -type f -name "part*" | grep guests -m ${MAX_FILES})
export SESSIONS_LIST=$(find snapshots-paddypower -type f -name 'part*' | grep sessions -m ${MAX_FILES})

COUNTER=0
for EVENT in $EVENTS_LIST
do
  cat $EVENT | \
    jq -c 'select(.type == "VIEW")' | \
    jq -c '.pointOfSale |= "paddypower.com"' | \
    jq -c '.clientKey |= "pPBFRdxAR61DXgGaYvvIPWQ7pAaq8QQJ"' > "$S3_PATH/events/$CLIENT_KEY/part-0000$COUNTER.events.json"
  let COUNTER=COUNTER+1
done

COUNTER=0
for GUEST in $GUESTS_LIST
do
  cat $GUEST | \
    jq -c '.pointOfSale |= "paddypower.com"' | \
    jq -c '.identifiers |= "[]"' | \
    jq -c '.clientKey |= "pPBFRdxAR61DXgGaYvvIPWQ7pAaq8QQJ"' > "$S3_PATH/guests/$CLIENT_KEY/part-0000$COUNTER.guests.json"
  let COUNTER=COUNTER+1
done

COUNTER=0
for SESSION in $SESSIONS_LIST
do
  cat $SESSION | \
    jq -c '.pointOfSale |= "paddypower.com"' | \
    jq -c '.clientKey |= "pPBFRdxAR61DXgGaYvvIPWQ7pAaq8QQJ"' > "$S3_PATH/sessions/$CLIENT_KEY/part-0000$COUNTER.sessions.json"
  let COUNTER=COUNTER+1
done

