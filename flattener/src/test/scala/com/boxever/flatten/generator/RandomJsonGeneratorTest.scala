package com.boxever.flatten.generator

import java.io.{File, PrintWriter}

import org.scalatest.FunSuite

class RandomJsonGeneratorTest extends FunSuite {

  test("testGenerate") {
    val gen = RandomJsonGenerator

    assert(gen.generate(2, 0).toString == """{"k0": "v", "k1": "v"}""")
    assert(gen.generate(2, 1).toString == """{"kkkkk0": {"kkkkk0": "v", "kkkkk1": "v"}, "kkkkk1": {"kkkkk0": "v", "kkkkk1": "v"}}""")
    assert(gen.generate(2, 2).toString == """{"kkkkk0": {"kkkkk0": {"kkkkk0": "v", "kkkkk1": "v"}, "kkkkk1": {"kkkkk0": "v", "kkkkk1": "v"}}, "kkkkk1": {"kkkkk0": {"kkkkk0": "v", "kkkkk1": "v"}, "kkkkk1": {"kkkkk0": "v", "kkkkk1": "v"}}}""")
  }

  test("testGeneratePrint") {
    val gen = RandomJsonGenerator
    println(gen.generate(1, 1).toString)

  }


  ignore("microbenchmark") {
    val gen = RandomJsonGenerator

    val start = System.nanoTime()
    val v = gen.generate(10, 6)
    val generate = System.nanoTime()
//    val jsonString = JSON.parseFull(v.toString)

    val parse = System.nanoTime()
    val writer = new PrintWriter(new File("/Users/telmofelix/workspace/snapshot-mocker/flattener/out/test/resources/snipplets/out.json"))
    writer.write(v.toString)
    writer.close()
    val end = System.nanoTime()

    println("gen: " + (generate - start))
    println("parse: " + (parse - generate))
    println("write: " + (end - parse))
  }

}
