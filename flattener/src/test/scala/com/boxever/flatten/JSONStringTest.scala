package com.boxever.flatten

import org.scalatest.FunSuite

class JSONStringTest extends FunSuite {

  test("testToString") {
    assert(JSONString().toString == "\"\"")
    assert(JSONString(-2).toString == "\"\"")
    assert(JSONString(1, "a").toString == "\"a\"")
    assert(JSONString(2, "a").toString == "\"aa\"")
    assert(JSONString(5, "a").toString == "\"aaaaa\"")

    val randomString = JSONString(5, random = true).toString
    assert(randomString.length == 7)
    assert(randomString.matches("""^\"\w*\"$"""))
  }

}
