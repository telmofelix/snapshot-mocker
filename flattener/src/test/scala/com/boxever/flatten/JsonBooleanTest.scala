package com.boxever.flatten

import org.scalatest.FunSuite

class JsonBooleanTest extends FunSuite {
  test("testToString") {
    assert(JSONBoolean().toString == "false")
    assert(JSONBoolean(false).toString == "false")
    assert(JSONBoolean(true).toString == "true")

  }
}
