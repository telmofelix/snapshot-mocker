package com.boxever.flatten

import org.scalatest.FunSuite

class JsonArrayTest extends FunSuite {
  test("testToString") {
    assert(JsonArray().toString == "[1]")
    assert(JsonArray(1).toString == "[1]")
    assert(JsonArray(2).toString == "[1, 1]")
    assert(JsonArray(5).toString == "[1, 1, 1, 1, 1]")

    assert(JsonArray(1, JSONString()).toString == """["a"]""")
    assert(JsonArray(2, JSONString()).toString == """["a", "a"]""")
    assert(JsonArray(5, JSONString()).toString == """["a", "a", "a", "a", "a"]""")

    assert(JsonArray(1, JSONBoolean()).toString == "[false]")
    assert(JsonArray(2, JSONBoolean()).toString == "[false, false]")
    assert(JsonArray(5, JSONBoolean()).toString == "[false, false, false, false, false]")

    assert(JsonArray(1, JSONBoolean(true)).toString == "[true]")
    assert(JsonArray(2, JSONBoolean(true)).toString == "[true, true]")
    assert(JsonArray(5, JSONBoolean(true)).toString == "[true, true, true, true, true]")

    assert(JsonArray(1, JSONNull()).toString == "[null]")
    assert(JsonArray(2, JSONNull()).toString == "[null, null]")
    assert(JsonArray(5, JSONNull()).toString == "[null, null, null, null, null]")

    // nested arrays testing
    assert(JsonArray(1, JsonArray()).toString == "[[1]]")
    assert(JsonArray(2, JsonArray()).toString == "[[1], [1]]")
    assert(JsonArray(5, JsonArray()).toString == "[[1], [1], [1], [1], [1]]")

    assert(JsonArray(1, JsonArray(3)).toString == "[[1, 1, 1]]")
    assert(JsonArray(2, JsonArray(3)).toString == "[[1, 1, 1], [1, 1, 1]]")
    assert(JsonArray(5, JsonArray(3)).toString == "[[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]]")

    assert(JsonArray(1, JsonArray(2, JSONString())).toString == """[["a", "a"]]""")
    assert(JsonArray(2, JsonArray(2, JSONString())).toString == """[["a", "a"], ["a", "a"]]""")
    assert(JsonArray(5, JsonArray(2, JSONString())).toString == """[["a", "a"], ["a", "a"], ["a", "a"], ["a", "a"], ["a", "a"]]""")


    //    assert(JsonArray("abacdefg").toString == "abacdefg")
//    assert(JsonArray("a", 2).toString == "aa")
//    assert(JsonArray("a", -2).toString == "a")
//    assert(JsonArray("a", 5).toString == "aaaaa")
  }
}
