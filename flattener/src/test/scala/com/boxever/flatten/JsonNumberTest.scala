package com.boxever.flatten

import org.scalatest.FunSuite

class JsonNumberTest extends FunSuite {
  test("testToString") {
    assert(JsonNumber().toString == "1")
    assert(JsonNumber(1).toString == "1")
    assert(JsonNumber(2).toString == "11")
    assert(JsonNumber(-2).toString == "1")
    assert(JsonNumber(5).toString == "11111")

    val randomNumber = JsonNumber(5, random = true).toString
    assert(randomNumber.length == 5 && randomNumber.forall(_.isDigit))
  }
}
