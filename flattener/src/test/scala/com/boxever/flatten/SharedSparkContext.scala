package com.boxever.flatten

import org.apache.spark.{LocalSparkContext, SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfterAll, Suite}

trait SharedSparkContext extends BeforeAndAfterAll { self: Suite =>
  @transient private var _sc: SparkContext = _

  def sc: SparkContext = _sc

  var conf = new SparkConf(false)

  override def beforeAll(): Unit = {
    _sc = new SparkContext("local[4]", "test", conf)
    super.beforeAll()
  }

  override def afterAll(): Unit = {
    LocalSparkContext.stop(_sc)
    _sc = null
    super.afterAll()
  }
}
