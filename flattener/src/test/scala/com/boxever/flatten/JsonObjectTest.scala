package com.boxever.flatten

import org.scalatest.FunSuite

class JSONObjectTest extends FunSuite {
  test("suffix") {
    assert(JSONObject().addSuffixToKey(JSONString(), 0).toString == """"0"""")
    assert(JSONObject().addSuffixToKey(JSONString(1), 0).toString == """"a0"""")
    assert(JSONObject().addSuffixToKey(JSONString(4), 0).toString == """"aaaa0"""")
  }

  test("testToString") {
    assert(JSONObject().toString == """{}""")
    assert(JSONObject(1).toString == """{"k0": "v"}""")
    assert(JSONObject(2).toString == """{"k0": "v", "k1": "v"}""")
    assert(JSONObject(5).toString == """{"k0": "v", "k1": "v", "k2": "v", "k3": "v", "k4": "v"}""")

    assert(JSONObject(5, JSONString(6, "k")).toString == """{"kkkkkk0": "v", "kkkkkk1": "v", "kkkkkk2": "v", "kkkkkk3": "v", "kkkkkk4": "v"}""")

    assert(JSONObject(5, JSONString(4, "n"), JSONNull()).toString == """{"nnnn0": null, "nnnn1": null, "nnnn2": null, "nnnn3": null, "nnnn4": null}""")
    assert(JSONObject(5, JSONString(4, "n"), JSONBoolean()).toString == """{"nnnn0": false, "nnnn1": false, "nnnn2": false, "nnnn3": false, "nnnn4": false}""")

    // objects of arrays
    assert(JSONObject(3, JSONString(4), JsonArray(2, JSONString(1))).toString == """{"aaaa0": ["a", "a"], "aaaa1": ["a", "a"], "aaaa2": ["a", "a"]}""")

    // objects of objects
    assert(JSONObject(3, JSONString(4, "o"), JSONObject(2)).toString == """{"oooo0": {"k0": "v", "k1": "v"}, "oooo1": {"k0": "v", "k1": "v"}, "oooo2": {"k0": "v", "k1": "v"}}""")

    assert(JSONObject(3, JSONString(4, "o"), JSONObject(2)).toString == """{"oooo0": {"k0": "v", "k1": "v"}, "oooo1": {"k0": "v", "k1": "v"}, "oooo2": {"k0": "v", "k1": "v"}}""")


    //assert(JSONObject(1, JSONString("kkkkk"), JSONObject(1, JSONString("aaaaa"))).toString == """{"kkkkk0": {"kkkkk0": 1}}""" )
  }
}
