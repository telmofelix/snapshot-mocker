package com.boxever.flatten

import org.scalatest.FunSuite

class ScalaFlatTestSuite extends FunSuite with SharedSparkContext {
  test("assert can parse basic arguments") {
    val args = Array("--config_file", "/tmp/myfile.txt", "--date", "2018/02/01")

    //val options = OptimaArgParser.getResolvedOptions(args)

    assert(2 == 2)

  }

  test("assert can ignore non arguments") {
    val inputRDD = sc.parallelize(Array("aaaaa", "bbbb", "cccc", "ddddd", "eeee"))

    val inputCount = inputRDD.count()

    assert(inputCount == 2)
  }

  test("glob up a file") {
    val sampleSessions = sc.textFile("file:///Users/telmofelix/workspace/snapshot-mocker/samples/sessions.json")

    //val options = OptimaArgParser.getResolvedOptions(args)

    assert(sampleSessions.count() == 92)

    sampleSessions.collect().foreach({json : String =>
      println(json.substring(0, 20))
    })
  }
}
