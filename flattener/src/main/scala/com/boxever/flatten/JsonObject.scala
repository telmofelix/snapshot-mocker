package com.boxever.flatten

import com.boxever.flatten.generator.RandomJSONValueGenerator

case class JSONObject(maxElements: Int = 0, objectKey: JSONString = JSONString(1, "k"), objectType: JSONValue = JSONString(1, "v"), random: Boolean = false) extends JSONValue {

  def addSuffixToKey(objectKey: JSONString, index: Int) = {
    objectKey.toString.substring(0, objectKey.toString.size - 1) + index + "\""
  }

  private def generateJSONValue(): JSONValue = {
    if(random) RandomJSONValueGenerator.randomJSONValue() else objectType
  }

  def generateString(maxElements: Int = 0) : String = {
    var i = 0;
    var objectValue = "{"
    while (i < maxElements) {
      objectValue = objectValue + addSuffixToKey(objectKey, i)  + ": " + generateJSONValue().toString
      i = i + 1
      if(i < maxElements) objectValue = objectValue + ", "
    }

    objectValue.concat("}")
  }

  override def toString: String = {
    val output = generateString(maxElements)

    output
  }
}
