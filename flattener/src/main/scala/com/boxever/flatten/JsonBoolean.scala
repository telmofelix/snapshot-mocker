package com.boxever.flatten

case class JSONBoolean(seed: Boolean = false) extends JSONValue {


  def generateString(seed: Boolean = false) : String = {
    seed.toString
  }

  override def toString: String = {
    val output = generateString(seed)

    output
  }
}
