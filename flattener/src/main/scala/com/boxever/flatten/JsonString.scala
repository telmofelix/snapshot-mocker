package com.boxever.flatten

import com.boxever.flatten.generator.CharGenerator

case class JSONString(length: Int = 0, seed: Seq[Char] = "a", random: Boolean = false) extends JSONValue {


  def generateString() : String = {
    var result = "\""

    val value = if(random) CharGenerator.randomAlpha(length) else CharGenerator.randomStringFromCharList(length, seed)
    result = result.concat(value)

    result + "\""
  }

  override def toString: String = {
    val output = generateString()

    output
  }
}
