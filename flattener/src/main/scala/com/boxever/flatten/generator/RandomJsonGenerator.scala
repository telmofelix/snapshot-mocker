package com.boxever.flatten.generator

import com.boxever.flatten.{JSONObject, JSONString, JSONValue}


object RandomJsonGenerator {


  def generate(maxAttributes: Int, maxDepth: Int, random: Boolean = true) : JSONValue = {
    var i = 0;
    if(maxDepth == 0) {
      val JSONValue = if(random) RandomJSONValueGenerator.randomJSONValue() else JSONObject(2)
      JSONObject(maxAttributes, JSONString(1, "l"), JSONValue)
    } else {
      val leafString = RandomJsonGenerator.generate(maxAttributes, maxDepth - 1);
      JSONObject(maxAttributes, JSONString(1, "k"), leafString)
    }
  }
}
