package com.boxever.flatten.generator

import com.boxever.flatten._

import scala.util.Random

object RandomJSONValueGenerator {

  val r: Random = scala.util.Random

  def randomJSONValue(maxElementsArray: Int = 2, maxElementsObject: Int = 2, lengthNumber: Int = 5, lengthString: Int = 4): JSONValue = {
    val i = r.nextInt(100)
    println(i)

    i match {
      case x if 0 until 10 contains x => JsonArray(maxElementsArray)
      case x if 11 until 40 contains x => JSONObject(maxElementsObject)
      case x if 41 until 50 contains x => JsonNumber(lengthNumber)
      case x if 61 until 90 contains x => JSONString(lengthString, "v")
      case x if 91 until 95 contains x => JSONBoolean()
      case _ => JSONNull()
    }
  }
}
