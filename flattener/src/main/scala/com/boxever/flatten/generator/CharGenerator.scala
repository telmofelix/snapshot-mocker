package com.boxever.flatten.generator

object CharGenerator {
  import scala.util.Random

  def uniqueRandomKey(chars: String, length: Int, uniqueFunc: String=>Boolean) : String =  {
    val newKey = (1 to length).map(
      x => {
        val index = Random.nextInt(chars.length)
        chars(index)
      }
    ).mkString("")

    if (uniqueFunc(newKey))
      newKey
    else
      uniqueRandomKey(chars, length, uniqueFunc)
  }
  /**
   * implement your own is unique here
   */
  def isUnique(s:String):Boolean = true


  // from https://alvinalexander.com/scala/creating-random-strings-in-scala

  // 7 - random numeric
  def randomNumeric(length: Int): String = {
    val chars = ('0' to '9')
    randomStringFromCharList(length, chars)
  }

  def randomAlpha(length: Int): String = {
    val chars = ('a' to 'z') ++ ('A' to 'Z')
    randomStringFromCharList(length, chars)
  }

  // 6 - random alphanumeric
  def randomAlphaNumericString(length: Int): String = {
    val chars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')
    randomStringFromCharList(length, chars)
  }

  def randomStringFromCharList(length: Int, chars: Seq[Char]): String = {
    val sb = new StringBuilder
    for (i <- 1 to length) {
      val randomNum = util.Random.nextInt(chars.length)
      sb.append(chars(randomNum))
    }
    sb.toString
  }


  def test() = {
    val chars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9') ++ ("-!£$")
    val key = uniqueRandomKey(chars.mkString(""), 8, isUnique)
    println(key)
  }
}
