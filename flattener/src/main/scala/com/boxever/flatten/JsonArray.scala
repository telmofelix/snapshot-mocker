package com.boxever.flatten

case class JsonArray(maxElements: Int = 1, arrayType: JSONValue = JsonNumber(), random: Boolean = false) extends JSONValue {


  def generateString() : String = {
    var i = 0;
    var arrayValue = "["
    while (i < maxElements) {
      arrayValue = arrayValue.concat(arrayType.toString)
      i = i + 1
      if(i < maxElements) arrayValue = arrayValue.concat(", ")
    }

    arrayValue.concat("]")
  }

  override def toString: String = {
    val output = generateString()

    output
  }
}
