package com.boxever.flatten

case class JSONNull() extends JSONValue {

  override def toString: String = {
   "null"
  }
}
