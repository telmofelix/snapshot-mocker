package com.boxever.flatten

trait JSONValue {
  def toString: String
}
