package com.boxever.flatten

import com.boxever.flatten.generator.CharGenerator

case class JsonNumber(length: Int = 1, random: Boolean = false, seed: Seq[Char] = Seq[Char]('1'))  extends JSONValue {
  override def toString: String = {
    val positiveLength = if (length > 0) length else 1

    val output =
      if (random) CharGenerator.randomNumeric(positiveLength)
      else CharGenerator.randomStringFromCharList(positiveLength, seed)

    output
  }
}
